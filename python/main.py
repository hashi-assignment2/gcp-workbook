from flask import Flask, render_template, request
from google.cloud import storage
import csv
import traceback
import os
from google.cloud.sql.connector import Connector, IPTypes
import pg8000
import json
import requests
import sqlalchemy

app = Flask(__name__)
 
def connect_with_connector() -> sqlalchemy.engine.base.Engine:
    """
    Initializes a connection pool for a Cloud SQL instance of Postgres.

    Uses the Cloud SQL Python Connector package.
    """
    # Note: Saving credentials in environment variables is convenient, but not
    # secure - consider a more secure solution such as
    # Cloud Secret Manager (https://cloud.google.com/secret-manager) to help
    # keep secrets safe.
    file = open("/vaultdata/postgres.txt", "r")
    username = str(file.readline()).strip()
    password = str(file.readline()).strip()

    instance_connection_name = os.environ[
        "INSTANCE_CONNECTION_NAME"
    ]  # e.g. 'project:region:instance'
    db_user = username  # e.g. 'my-db-user'
    db_pass = password  # e.g. 'my-db-password'
    db_name = os.environ["DB_NAME"]  # e.g. 'my-database'

    ip_type = IPTypes.PRIVATE if os.environ.get("PRIVATE_IP") else IPTypes.PUBLIC

    # initialize Cloud SQL Python Connector object
    connector = Connector()

    def getconn() -> pg8000.dbapi.Connection:
        conn: pg8000.dbapi.Connection = connector.connect(
            instance_connection_name,
            "pg8000",
            user=db_user,
            password=db_pass,
            db=db_name,
            ip_type=ip_type,
        )
        return conn

    # The Cloud SQL Python Connector can be used with SQLAlchemy
    # using the 'creator' argument to 'create_engine'
    pool = sqlalchemy.create_engine(
        "postgresql+pg8000://",
        creator=getconn,
        # ...
    )

    return pool

@app.route('/')
def hello_world():
    return "Hello world"

@app.route('/encrypt')
def fetch_details():
    try:
        a = []
        file = open("/vaultdata/postgres.txt", "r")
        username = str(file.readline()).strip()
        password = str(file.readline()).strip()
        a.append(username)
        a.append(password)

        file = open("/secret/text.txt", "r")
        token = str(file.read())

        card = request.args.get('card')
        
        encodeUrl = "http://localhost:8200/v1/transform/encode/payments"

        data = {
            "value": str(card),
            "transformation": "ccn-fpe"
        }

        dataMask = {
            "value": str(card),
            "transformation": "ccn-masking"
        }
        headers = {
            "X-Vault-Namespace": "admin"
        }
        response = requests.post(encodeUrl,data=data,headers=headers)
        encrypted = response.json()["data"]["encoded_value"]
        a.append(encrypted)

        response = requests.post(encodeUrl,data=dataMask,headers=headers)
        a.append(response.json()["data"]["encoded_value"])

        pool = connect_with_connector()
        with pool.connect() as db_conn:
            insert_stmt = sqlalchemy.text(
                "INSERT INTO CARDS (name, card, cardenc) VALUES (:name, :card, :cardenc)"
            )

            db_conn.execute(insert_stmt, parameters={"name": "shriram", "card": str(card), "cardenc": encrypted })
            db_conn.commit()

            result = db_conn.execute(sqlalchemy.text("SELECT * FROM CARDS")).fetchall()
            for row in result:
                a.append(str(row))

            return a
    except Exception as e:
        print(e)
        print(traceback.format_exc())
        return traceback.format_exc()

@app.route('/decrypt')
def decrypt_details():
    try:
        a = []
        file = open("/vaultdata/postgres.txt", "r")
        username = str(file.readline()).strip()
        password = str(file.readline()).strip()
        a.append(username)
        a.append(password)

        file = open("/secret/text.txt", "r")
        token = str(file.read())

        card = request.args.get('card')
        
        decodeUrl = "http://localhost:8200/v1/transform/decode/payments"

        data = {
            "value": str(card),
            "transformation": "ccn-fpe"
        }

        headers = {
            "X-Vault-Namespace": "admin"
        }
        response = requests.post(decodeUrl,data=data,headers=headers)
        a.append(response.json()["data"]["decoded_value"])

        pool = connect_with_connector()
        with pool.connect() as db_conn:
            result = db_conn.execute(sqlalchemy.text("SELECT * FROM CARDS")).fetchall()
            for row in result:
                a.append(str(row))
            
            return a

    except Exception as e:
        print(e)
        print(traceback.format_exc())
        return traceback.format_exc()


@app.route('/decrypt-lastfour')
def decrypt_details_lastfour():
    try:
        a = []
        file = open("/vaultdata/postgres.txt", "r")
        username = str(file.readline()).strip()
        password = str(file.readline()).strip()
        a.append(username)
        a.append(password)

        file = open("/secret/text.txt", "r")
        token = str(file.read())

        card = request.args.get('card')
        
        decodeUrl = "http://localhost:8200/v1/transform/decode/payments/last-four"

        data = {
            "value": str(card),
            "transformation": "ccn-fpe"
        }

        headers = {
            "X-Vault-Namespace": "admin"
        }
        response = requests.post(decodeUrl,data=data,headers=headers)
        a.append(response.json()["data"]["decoded_value"])

        pool = connect_with_connector()
        with pool.connect() as db_conn:
            result = db_conn.execute(sqlalchemy.text("SELECT * FROM CARDS")).fetchall()
            for row in result:
                a.append(str(row))
            
            return a


    except Exception as e:
        print(e)
        print(traceback.format_exc())
        return traceback.format_exc()


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)
