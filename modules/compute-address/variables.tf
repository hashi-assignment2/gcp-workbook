variable "address_name" {
  type = string
}

variable "address_value" {
  type    = string
  default = ""
}

variable "address_type" {
  type    = string
  default = "EXTERNAL"
}

variable "description" {
  type    = string
  default = "Default description"
}

variable "address_purpose" {
  type    = string
  default = "GCE_ENDPOINT"
}

variable "address_network_tier" {
  type    = string
  default = "PREMIUM"
}

variable "address_subnetwork" {
  type    = string
  default = ""
}

variable "address_labels" {
  type    = list(map(string))
  default = [{}]
}

variable "address_region" {
  type    = string
  default = "us-central1"
}


