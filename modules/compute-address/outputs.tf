output "eip_self_link" {
  value = google_compute_address.address.self_link
}

output "address" {
  value = google_compute_address.address.address
}