resource "google_container_node_pool" "node-pool" {
  name    = var.cluster_node_pool_name
  cluster = var.cluster_name
  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    service_account = var.sa_email             //create a service account and link it here
    machine_type    = var.cluster_machine_type //e2-standard-2
    tags            = var.cluster_tags         //list
  }

  initial_node_count = 1 //3
  max_pods_per_node  = 110
  autoscaling {
    min_node_count = 1
    max_node_count = 3
  }
  timeouts {
    create = "30m"
    update = "20m"
  }
}
