#!/bin/bash
# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

set -x

# Install necessary dependencies
sudo apt-get update -y
# sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade
# sudo apt-get update
sudo apt-get -y -qq install wget unzip
sudo mkdir /opt/tfc-agent
sudo wget https://releases.hashicorp.com/tfc-agent/1.13.0/tfc-agent_1.13.0_linux_amd64.zip && sudo cp tfc-agent_1.13.0_linux_amd64.zip /opt/tfc-agent/tfc-agent_1.13.0_linux_amd64.zip && cd /opt/tfc-agent && ls && sudo unzip tfc-agent_1.13.0_linux_amd64.zip
export TFC_AGENT_TOKEN="TFCTOKEN"
export TFC_AGENT_NAME="gke-agentpool"
sudo tee -a tfc-agent.env > /dev/null <<EOF
TFC_AGENT_TOKEN="TFCTOKEN" 
TFC_AGENT_NAME="gke-agentpool" 
EOF
sudo cp tfc-agent.env /opt/tfc-agent/tfc-agent.env

sudo tee -a tfc-agent.service > /dev/null <<EOT
[Unit] 
Description=Service to automatically start TFC/E Agent 
After=network.target 

[Install] 
WantedBy=multi-user.target 

[Service]
EnvironmentFile=/opt/tfc-agent/tfc-agent.env
Type=simple 
ExecStart=/opt/tfc-agent/tfc-agent 
KillSignal=SIGINT 
WorkingDirectory=/opt/tfc-agent 
Restart=always 
RestartSec=5 
StandardOutput=syslog 
StandardError=syslog 
SyslogIdentifier=%n 
EOT

sudo cp tfc-agent.service /etc/systemd/system/tfc-agent.service


sudo systemctl daemon-reload
sudo systemctl enable tfc-agent
sudo systemctl start tfc-agent
sudo systemctl status tfc-agent
