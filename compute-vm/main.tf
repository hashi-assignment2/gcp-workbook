data "hcp_packer_image" "tf-agent-image" {
  bucket_name = "packer-terraform-images"
  channel = "latest"
  cloud_provider = "gce"
  region = "us-central1-b"
}
resource "google_compute_instance" "tfc-agent" {
    name = "tfc-agent"
    machine_type = "n1-standard-1"
    zone = "us-central1-b"

    boot_disk {
      initialize_params {
        image = "projects/${var.project_id}/global/images/${data.hcp_packer_image.tf-agent-image.cloud_image_id}"
      }
    }

    tags = ["ssh"]

    network_interface {
      network = "gke-network"
      subnetwork = "subnet-1"

      access_config {
        
      }
    }
}

resource "google_compute_instance" "vm2" {
    name = "delete-vm-drift"
    machine_type = "n1-standard-1"
    zone = "us-central1-b"

    boot_disk {
      initialize_params {
        image = "projects/${var.project_id}/global/images/${data.hcp_packer_image.tf-agent-image.cloud_image_id}"
      }
    }

    network_interface {
      network = "gke-network"
      subnetwork = "subnet-1"
    }
}

check "health_check" {
  data "http" "pythonapp" {
    url = "https://pythonapplication.endpoints.shriram-poc-project1.cloud.goog/healthcheck"
  }

  assert {
    condition = data.http.pythonapp.status_code == 200
    error_message = "${data.http.pythonapp.url} returned an unhealthy status code"
  }
}
