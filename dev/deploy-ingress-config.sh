#!/bin/bash
gcloud container clusters get-credentials $2 --zone $3-c --project $1
kubectl apply -f cert.yaml
kubectl apply -f ingress-frontendconfig.yaml