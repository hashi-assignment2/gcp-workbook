# -------------- providers.tf

variable "service_account" {
  type = string
}

variable "project_region" {
  type    = string
  default = "us-central1"
}

variable "project_id" {
  type = string
}


#Compute Network
variable "vpc_name" {

}

variable "vpc_auto_create_subnetworks" {

}

#Compute Subnetwork

variable "subnet_name" {
  type = string
}

variable "subnet_ip_cidr_range" {
  type = string
}

variable "subnet_region" {
  type = string
}

variable "subnet_description" {
  type    = string
  default = ""
}

variable "subnet_secondary_ip_range" {
  type    = map(string)
  default = {}
}

variable "subnet_private_ip_google_access" {
  type    = bool
  default = true
}

variable "subnet_log_config" {
  type    = map(string)
  default = {}
}

#Compute router
variable "router_name" {
  type = string
}

variable "router_region" {
  type = string
}

#Compute Address
variable "address_name" {
  type = string
}

#Compute ROuter Nat
variable "nat_name" {
  type = string
}

variable "source_subnetwork_ip_ranges_to_nat" {
  type = string
}

variable "nat_ip_allocate_option" {
  type = string
}

variable "nat_subnetwork" {
  type = map(any)
}

# GKE.tf

variable "cluster_name" {
  type    = string
  default = "gke-cluster"
}

variable "cluster_man_rules" {
  type = list(map(string))
}

variable "cluster_node_pool_name" {
  type    = string
  default = "nodepool"
}

variable "cluster_initial_node_count" {
  type    = number
  default = 1
}

variable "ksa_name" {
  type    = string
  default = "ksa"
}

# --- IAM.tf
variable "gke_sa_name" {
  type    = string
  default = "k8s-sa"
}

variable "remote_builders_sa_name" {
  type    = string
  default = "remote-builders-sa"
}

# storage.tf

variable "bucket_name" {
  type = string
}

variable "object_name" {
  type = string
}

# k8s.tf

variable "deployment_name" {
  type    = string
  default = "pythonapp"
}

variable "ingress_name" {
  type    = string
  default = "pythonapp"
}

variable "ssl_policy_name" {
    type = string
    default = "ssl-policy2"
}

variable "ssl_tls_version" {
  type    = string
  default = "TLS_1_2"
}

variable "ssl_profile" {
  type    = string
  default = "CUSTOM"
}

variable "ssl_custom_features" {
  type = list(string)
  default = ["TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256",
    "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
    "TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256",
    "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
    "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
    "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256",
    "TLS_RSA_WITH_AES_128_GCM_SHA256",
    "TLS_RSA_WITH_AES_256_GCM_SHA384"
  ]
}

variable "instance_connection_name" {
  type = string
}

variable "db_user" {
  type = string
}

variable "db_pass" {
  type = string
}

variable "db_name" {
  type = string
}

variable "vault_auth_token" {
  type = string
}

variable "vault_addr" {
  type = string
}