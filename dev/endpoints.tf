resource "google_endpoints_service" "endpoint_service" {
  service_name = "pythonapplication.endpoints.${var.project_id}.cloud.goog"
  project      = var.project_id
  openapi_config = templatefile(
    "${path.module}/openapi-service.yaml.tpl",
    {
      hostname  = "pythonapplication.endpoints.${var.project_id}.cloud.goog"
      static_ip = "${google_compute_global_address.ingress-ip.address}"
    }
  )

  depends_on = [
    google_compute_global_address.ingress-ip
  ]
}

resource "google_compute_managed_ssl_certificate" "cert" {
  name    = "cert-production"
  project = var.project_id

  managed {
    domains = ["pythonapplication.endpoints.${var.project_id}.cloud.goog"]
  }
}
