exit_after_auth = false

auto_auth {
    method {
        type = "token_file"

        config = {
            token_file_path = "/secret/text.txt"
        }
    }

    sink "file" {
        config = {
            path = "/home/vault/.vault-token"
        }
    }

}

template {
    destination = "/vaultdata/secret.txt"
    contents = <<EOT
    {{- with secret "kv/data/details" }}
    name: {{ .Data.data.name }}
    phone: {{ .Data.data.phone }}
    {{ end }}
    EOT
}

template {
    destination = "/vaultdata/postgres.txt"
    contents = <<EOT
    {{- with secret "database/creds/readonly" -}}
    {{ .Data.username }}
    {{ .Data.password }}
    {{- end -}}
    EOT
}

cache {
  use_auto_auth_token = true
}

listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = true
}
