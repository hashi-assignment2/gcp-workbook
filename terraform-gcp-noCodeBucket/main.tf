provider "google" {
  region = "us-central1"
}

resource "random_string" "random" {
  length    = 10
  min_lower = 10
  numeric   = false
  special   = false
}

resource "google_storage_bucket" "bucket" {
  prefix          = "${var.name}-${random_string.random.result}"
  location      = var.location
  storage_class = var.storage_class
  versioning {
    enabled = var.versioning
  }
}